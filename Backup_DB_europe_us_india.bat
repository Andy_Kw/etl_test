REM backup script for postgresql dbname
REM 13/12/16
REM creates a tar dump with a timestamp


if "%time:~0,1%" == " "  (set hour=0%time:~1,1%) else (set hour=%time:~0,2%)

set timestamp=%date:~-10,2%%date:~-7,2%%date:~-4,4%_%hour%%time:~3,2%

echo %timestamp%

REM it creates a sorting space : K:\BI\backup\sort

mkdir K:\BI\backup\sort

"C:\Program Files\PostgreSQL\9.3\bin\pg_dump.exe" -Ft --dbname=postgresql://avocarbon:AV0CARB0N?@127.0.0.1:5432/BI_DB > K:\BI\backup\sort\bck_%timestamp%.tar

REM the error level checks if the pg_dump(backup was done correctly)
REM if yes, it will write Backup done correctly
REM if no, it will write Issues with backup 

if errorlevel 0 (echo Backup done correctly >  K:\BI\backup\log\log%timestamp%.log)
if errorlevel 1 (echo Issues with backup >  K:\BI\backup\log\log%timestamp%.log)

REM we zip the tar file once the backup is over

"C:\Program Files\7-Zip\7z.exe" a K:\BI\backup\zip\bck%timestamp%.zip K:\BI\backup\sort\*

REM the error level checks if the zip was done correctly
REM if yes, it will write Compress done correctly 
REM if no, it will write Issues with compress

if errorlevel 0 (echo Compress done correctly >>  K:\BI\backup\log\log%timestamp%.log)
if errorlevel 1 (echo Issues with compress >>  K:\BI\backup\log\log%timestamp%.log)

REM check if the the zip file is here, if so it will delete the tar file. If no, it will do nothing

if exist K:\BI\backup\zip\bck%timestamp%.zip del K:\BI\backup\sort\bck_%timestamp%.tar

REM it deletes the sorting space : K:\BI\backup\sort

rmdir K:\BI\backup\sort


